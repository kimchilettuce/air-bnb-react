import React from "react";
import "./Navbar.css";

import logo from "../assets/airbnb-logo.svg";

export default function Navbar() {
    return (
        <div className="nav">
            <img className="nav--logo" src={logo} alt="Logo Airbnb" />
        </div>
    );
}
