import React from "react";
import "./Hero.css";

import imgGroup from "../assets/Image-group.svg";

export default function Hero() {
    return (
        <section className="hero">
            {/**I've set the overarching section as a flexbox */}

            <img
                src={imgGroup}
                alt="Group Display"
                className="hero--image-group"
            />

            {/**I'm trying to make these elements below left aligned in the flex-box */}
            <div className="hero--text">
                <h1 className="hero--text-title">Online Experiences</h1>
                <h2 className="hero--text-body">
                    Join unique interactive activities led by one-of-a-kind
                    hosts—all without leaving home.
                </h2>
            </div>
        </section>
    );
}
